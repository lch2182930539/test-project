import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('http://18.206.156.168:8087/')

WebUI.setText(findTestObject('Object Repository/Page_ProjectBackend/input_Username_username'), 'admin')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_ProjectBackend/input_Password_password'), 'RAIVpflpDOg=')

WebUI.click(findTestObject('Object Repository/Page_ProjectBackend/button_Login'))

WebUI.click(findTestObject('Object Repository/Page_ProjectBackend/a_Total Transaction'))

WebUI.verifyElementText(findTestObject('Page_ProjectBackend/Bananas'), 'Banana')

WebUI.verifyElementText(findTestObject('Page_ProjectBackend/150 THB'), '150 THB')

WebUI.verifyElementText(findTestObject('Object Repository/Page_ProjectBackend/p_Total price  162040 THB'), 'Total price: 162,040 THB')

WebUI.closeBrowser()

